title: Install Graphics Libraries
---
## About Graphics libraries

Playing Roblox may result in a black or white screen, an empty skybox upon loading up, a "graphics card not compatible" pop-up, or a crash.

To fix this, 32-bit graphics libraries are required for certain distributions.

## Installing Graphics libraries

### Debian 10/11/Testing

See [Debian Wiki: Graphics Card](https://wiki.debian.org/GraphicsCard)

### Ubuntu / Pop!_OS / Mint or other Ubuntu derivatives

- NVIDIA: `sudo add-apt-repository ppa:graphics-drivers/ppa && sudo dpkg --add-architecture i386 && sudo apt update && sudo apt install -y nvidia-driver-515 libvulkan1 libvulkan1:i386`

- Intel / AMD: `sudo add-apt-repository ppa:kisak/kisak-mesa && sudo dpkg --add-architecture i386 && sudo apt update && sudo apt upgrade && sudo apt install libgl1-mesa-dri:i386 mesa-vulkan-drivers mesa-vulkan-drivers:i386`

### Arch / Manjaro / Endeavour or other Arch derivatives

:warning: Make sure to enable 32-bit support see: [multilib repository](https://wiki.archlinux.org/title/Official_repositories#multilib).


- NVIDIA: `sudo pacman -S --needed lib32-nvidia-utils vulkan-icd-loader lib32-vulkan-icd-loader`  
- AMD: `sudo pacman -S --needed lib32-mesa vulkan-radeon lib32-vulkan-radeon vulkan-icd-loader lib32-vulkan-icd-loader`
- Intel: `sudo pacman -S --needed lib32-mesa vulkan-intel lib32-vulkan-intel vulkan-icd-loader lib32-vulkan-icd-loader`

### Fedora

dnf will pull the required graphics driver required by your system already.

To install Vulkan support: `sudo dnf install vulkan-loader vulkan-loader.i686`

### OpenSUSE

NVIDIA: The closed source NVIDIA driver is not available by default.
For Vulkan support: `sudo zypper in libvulkan1 libvulkan1-32bit`

- AMD: `sudo zypper in kernel-firmware-amdgpu libdrm_amdgpu1 libdrm_amdgpu1-32bit libdrm_radeon1 libdrm_radeon1-32bit libvulkan_radeon libvulkan_radeon-32bit libvulkan1 libvulkan1-32bit`
- Intel: `sudo zypper in kernel-firmware-intel libdrm_intel1 libdrm_intel1-32bit libvulkan1 libvulkan1-32bit libvulkan_intel libvulkan_intel-32bit`

### NixOS

Set `hardware.opengl.driSupport32Bit = true;` in `configuration.nix` to get 32-bit GPU drivers.
The `grapejuice` package will draw in all other dependencies.

### Void Linux

See [Void Linux Handbook: Graphic Drivers](https://docs.voidlinux.org/config/graphical-session/graphics-drivers/index.html)
